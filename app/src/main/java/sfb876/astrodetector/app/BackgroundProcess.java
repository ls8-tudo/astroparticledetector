package sfb876.astrodetector.app;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

import de.greenrobot.event.EventBus;
import stream.run;

/**
 * Background process that should host the streams framework process
 * and give some response on the progress (found events)
 * Created by alexey on 18.06.14.
 */
public class BackgroundProcess extends IntentService {

    final static Logger log = LoggerFactory.getLogger(BackgroundProcess.class);

    public static boolean stopped;
    private Thread thread;
    private static boolean eventBusSet = false;

    /**
     * Creates an IntentService to host the background process.
     * With the onDestroy event this service can be stopped.
     * EventBus is used for several background services (even
     * after destroying one).
     */
    public BackgroundProcess() {
        super(Constants.TAG);
        stopped = false;
        if (!eventBusSet) {
            EventBus.getDefault().register(this);
            eventBusSet = !eventBusSet;
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();
        String chosenProcess = (String) extras.get(Constants.PROCESS_NAME);
        final URL url = BackgroundProcess.class.getResource(chosenProcess);
        Runnable runStream = new Runnable() {
            @Override
            public void run() {
            try {
                run.main(url);
            } catch (Exception e) {
                sendMessageToCaller("Process ended with exception:\n" + e.toString());
                e.printStackTrace();
            }
            }

        };
        thread = new Thread(runStream);
        thread.start();
        sendMessageToCaller("Process started successfully: \n"+url.toString());

        try {
            thread.join();
        } catch (InterruptedException e) {
            sendMessageToCaller("thread exception while sleep:\n"+e.toString());
            e.printStackTrace();
        } finally {
            log.debug("Thread stopped!");
        }
    }

    private void sendMessageToCaller(String message) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Constants.PROCESS_RESPONSE);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(Constants.RESPONSE, message);
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopped = true;
        thread.interrupt();
        sendMessageToCaller("Process stopped!");
    }

    public void onEvent(DetectionEvent event){
        sendMessageToCaller(event.getMessage());
    }
}