package sfb876.astrodetector.app;

import android.annotation.TargetApi;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;


import java.util.List;

import sfb876.astrodetector.R;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceFragment{
    public static final String PICTURE_SIZES = "picture_sizes";
    public static final String FOLDER_PATH = "folder_path";
    public static final String USE_THREADS = "use_threads";
    public static final String JPEG_COMPRESSION = "jpeg_compression";
    public static final String USE_LOGGING = "use_logging";
    public static final String LINE_THRESHOLD = "line_threshold";
    public static final String LINE_DETECT_METHOD = "line_detect_method";
    public static List<Camera.Size> sizes;

    private static final int
            STRING_MODE = 0,
            BOOL_MODE = 1,
            INT_MODE = 2,
            LONG_MODE = 3,
            FLOAT_MODE = 4;

    static String[] compressions = {"10", "30", "50", "70", "90", "100"};

    static String[] lineDetectMethods = {"Average distance",
            "Median of all distances",
            "Variance of distances"};

    public static void init(Camera.Parameters parameters){
        sizes = parameters.getSupportedPictureSizes();
        Constants.PICTURE_SIZE = sizes.size()-1;
        Constants.JPEG_COMPRESSION = compressions.length-1;
        Constants.LINE_THRESHOLD = 10;
        Constants.LINE_DETECT_METHOD = 0;
    }

    /**
     * Shows the simplified settings UI if the device configuration if the
     * device configuration dictates that a simplified, single-pane UI should be
     * shown.
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // In the simplified UI, fragments are not used at all and we instead
        // use the older PreferenceActivity APIs.

        addPreferencesFromResource(R.xml.pref_general);
        ListPreference listPreference = (ListPreference) findPreference(PICTURE_SIZES);
        if (listPreference!=null) {
            CharSequence entries[] = new String[sizes.size()];
            CharSequence entryValues[] = new String[sizes.size()];
            int i = 0;
            for(Camera.Size size : sizes){
                entries[i] = size.width + "x" + size.height;
                entryValues[i++] = Integer.toString(i);
            }
            listPreference.setEntries(entries);
            listPreference.setEntryValues(entryValues);
            listPreference.setDefaultValue(sizes.size()-1);
        }

        listPreference = (ListPreference) findPreference(JPEG_COMPRESSION);
        if (listPreference!=null){

            CharSequence entries[] = new String[compressions.length];
            short i = 0;
            for(String value : compressions){
                entries[i++] = value;
            }
            listPreference.setEntries(entries);
            listPreference.setEntryValues(entries);
            listPreference.setDefaultValue(compressions.length-1);
        }

        listPreference = (ListPreference) findPreference(LINE_DETECT_METHOD);
        if (listPreference!=null) {
            CharSequence entries[] = new String[lineDetectMethods.length];
            CharSequence entryValues[] = new String[lineDetectMethods.length];
            int i = 0;
            for(String s : lineDetectMethods){
                entries[i] = s;
                entryValues[i] = Integer.toString(i);
                i++;
            }
            listPreference.setEntries(entries);
            listPreference.setEntryValues(entryValues);
            listPreference.setDefaultValue(0);
        }

        // Bind the summaries of EditText/List/Dialog/Ringtone preferences to
        // their values. When their values change, their summaries are updated
        // to reflect the new value, per the Android Design guidelines.
        bindPreferenceSummaryToValue(findPreference(FOLDER_PATH), STRING_MODE);
        bindPreferenceSummaryToValue(findPreference(PICTURE_SIZES), STRING_MODE);
        bindPreferenceSummaryToValue(findPreference(USE_THREADS), BOOL_MODE);
        bindPreferenceSummaryToValue(findPreference(USE_LOGGING), BOOL_MODE);
        bindPreferenceSummaryToValue(findPreference(JPEG_COMPRESSION), STRING_MODE);
        bindPreferenceSummaryToValue(findPreference(LINE_THRESHOLD), STRING_MODE);
        bindPreferenceSummaryToValue(findPreference(LINE_DETECT_METHOD), STRING_MODE);
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference, int MODE) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        switch (MODE){
            case STRING_MODE:
                String s = preference.getKey();
                Object value = PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(s, "");
                sBindPreferenceSummaryToValueListener.onPreferenceChange(preference, value);
                break;
            case BOOL_MODE:
                sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.getContext())
                                .getBoolean(preference.getKey(), true));
                break;
            case INT_MODE:
                sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.getContext())
                                .getString(preference.getKey(), ""));
                break;
            case LONG_MODE:
                sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.getContext())
                                .getLong(preference.getKey(), 0));
                break;
            case FLOAT_MODE:
                sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.getContext())
                                .getFloat(preference.getKey(), 1f));
                break;
        }
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference(FOLDER_PATH), STRING_MODE);
            bindPreferenceSummaryToValue(findPreference(PICTURE_SIZES), STRING_MODE);
            bindPreferenceSummaryToValue(findPreference(USE_THREADS), BOOL_MODE);
            bindPreferenceSummaryToValue(findPreference(USE_LOGGING), BOOL_MODE);
            bindPreferenceSummaryToValue(findPreference(JPEG_COMPRESSION), STRING_MODE);
            bindPreferenceSummaryToValue(findPreference(LINE_THRESHOLD), STRING_MODE);
            bindPreferenceSummaryToValue(findPreference(LINE_DETECT_METHOD), STRING_MODE);
        }
    }
}
