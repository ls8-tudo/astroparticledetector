package sfb876.astrodetector.app;

import de.greenrobot.event.EventBus;

/**
 * Created by Matuschek on 21.08.2014.
 */
public class DetectionEvent {

    String msg;

    public DetectionEvent(){
        this.msg = "";
    }

    public String getMessage(){
        return msg;
    }

    public void setMessage(String msg){
        this.msg = msg;
    }

    public void postMessage(String msg){
        this.msg = msg;
        EventBus.getDefault().post(this);
    }

    public void logMessage(String msg){
        if (Constants.LOGGING){
            postMessage(msg);
        }
    }
}
