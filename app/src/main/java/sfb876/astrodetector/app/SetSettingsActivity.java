package sfb876.astrodetector.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by alexey on 12.09.14.
 */
public class SetSettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsActivity()).commit();
        setResult(2);
    }

    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent();
        setResult(RESULT_OK, mIntent);
        super.onBackPressed();
    }
}
