package sfb876.astrodetector.app;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.commonsware.cwac.camera.CameraFragment;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import sfb876.astrodetector.R;
import sfb876.astrodetector.streams.io.AndroidCameraHost;
import sfb876.astrodetector.streams.io.AndroidCameraStream;
import sfb876.astrodetector.streams.io.AndroidMJpegImageStream;
import sfb876.astrodetector.streams.io.TakePicturesProcess;

public class StartActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener{

    // Android view
    ProgressDialog pd;
    Context context;
    Button startButton;
    Button stopButton;
    CheckBox imageProcessing;
    Spinner dropdownXml;
    TextView resultTextView;
    ScrollView resultScrollView;
    Intent resultViewIntent;
    BroadcasterReceiverTextView broadcasterReceiverTextView;
    CameraFragment f;
    RelativeLayout pageLoading;
    IntentFilter filter;

    // Helper variables
    String chosenXML = "";
    private boolean runningState = false;
    TakePicturesProcess picturesProcess;
    int volume = 0;
    double batteryLevel = 0;

    IntentFilter iF = null;
    Intent batIntent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        context = this;

        initBatIntent();

        // Init Log4J support for android
        ConfigureLog4J.configure();

        // Init camera fragment
        setUpCameraStream();

        pageLoading = (RelativeLayout) findViewById(R.id.main_layoutPageLoading);
        filter = new IntentFilter(Constants.PROCESS_RESPONSE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        broadcasterReceiverTextView = new BroadcasterReceiverTextView();

        List<String> stringList = getXMLFiles();
        dropdownXml = (Spinner) findViewById(R.id.spinner_xml);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdownXml.setAdapter(adapter);
        dropdownXml.setOnItemSelectedListener(this);

        resultTextView = (TextView) findViewById(R.id.result_scroll_text_view);
        resultScrollView = (ScrollView) findViewById(R.id.result_scroll_view);

        resultViewIntent = new Intent(this, BackgroundProcess.class);

        startButton = (Button) findViewById(R.id.start_button);
        startButton.setOnClickListener(startListener);

        stopButton = (Button) findViewById(R.id.stop_button);
        stopButton.setEnabled(false);
        stopButton.setOnClickListener(stopListener);

        imageProcessing = (CheckBox) findViewById(R.id.choose_image_processing);
        imageProcessing.setChecked(true);
        imageProcessing.setOnCheckedChangeListener(imageChangeListener);
    }

    /**
     * Retrieve all XML files which can be found in resources folder.
     * @return
     */
    public ArrayList<String> getXMLFiles(){
        ArrayList<String> stringList = new ArrayList<String>();
        try {
            final AssetManager mgr = getAssets();
            String[] list = mgr.list("/");
            if(list!=null){
                for(String str : list){
                    if (str.endsWith(".xml") && !str.equals("AndroidManifest.xml")){
                        stringList.add(str);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringList;
    }

    /**
     * onClickListener for start button
     */
    View.OnClickListener startListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!runningState) {
                if (imageProcessing.isChecked()) {
                    if (!Constants.PREVIEW_RESTARTED){
                        f.restartPreview();
                    }
                    //Turn off sound
                    AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    volume = manager.getStreamVolume(AudioManager.STREAM_SYSTEM);
                    manager.setStreamVolume(AudioManager.STREAM_SYSTEM, 0 , AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

                    //Start taking pictures
                    picturesProcess = new TakePicturesProcess();
                    picturesProcess.setStartActivity((StartActivity) context);
                    picturesProcess.execute();
                }
                // start the processing
                // method will add output log onto it
                batteryLevel = getApproxBatLevel();
                Bundle extras = new Bundle();
                extras.putString(Constants.PROCESS_NAME, File.separator + chosenXML);
                resultViewIntent.putExtras(extras);
                registerReceiver(broadcasterReceiverTextView, filter);
                startService(resultViewIntent);
                switchEnabledButtons();
                runningState = true;
            }
        }
    };

    /**
     * onClickListener for stop button
     */
    View.OnClickListener stopListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (runningState) {
                if (imageProcessing.isChecked()) {
                    picturesProcess.cancel(true);
                }
                AndroidCameraStream.stopCameraStream = true;
                AndroidMJpegImageStream.stopJpegStream = true;
                showLoadingAnimation();
            }
        }
    };

    /**
     * onCheckedChangeListener for image processing check box
     */
    CompoundButton.OnCheckedChangeListener imageChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
                loadCameraFragment(true);
            }else{
                getFragmentManager().beginTransaction()
                        .remove(f).commit();
            }
        }
    };

    /**
     * Take some time for camera to reload after it has been switched of
     * for processing data from storage.
     * @param add true if the view has been removed
     */
    void loadCameraFragment(final boolean add){
        startButton.setEnabled(false);
        imageProcessing.setEnabled(false);
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                pd = new ProgressDialog(context);
                pd.setTitle(Constants.CAMERA_LOAD_TITLE);
                pd.setMessage(Constants.CAMERA_LOAD_MESSAGE);
                pd.setCancelable(false);
                pd.setIndeterminate(true);
                pd.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                if (add) {
                    getFragmentManager().beginTransaction()
                            .add(R.id.container, f, Constants.TAG_CAMERA_FRAGMENT)
                            .commit();
                }
                try {
                    Thread.sleep(Constants.CAMERA_LOAD_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if(pd!=null){
                    pd.dismiss();
                    startButton.setEnabled(true);
                    imageProcessing.setEnabled(true);
                }
            }
        };
        task.execute();
    }

    public CameraFragment getCameraFragment() {
        return f;
    }

    /**
     * Set up everything needed for App Settings and CWAC camera library to take pictures.
     */
    private void setUpCameraStream(){
        // Init camera parameters for settings menu
        Camera camera = Camera.open();
        Camera.Parameters parameters = camera.getParameters();
        SettingsActivity.init(parameters);
        camera.release();

        //Create the CameraFragment and add it to the layout
        f = new CameraFragment();
        getFragmentManager().beginTransaction()
                .add(R.id.container, f, Constants.TAG_CAMERA_FRAGMENT)
                .commit();

        //Set the CameraHost
        final AndroidCameraHost.Builder b = new AndroidCameraHost.Builder(new AndroidCameraHost(this));
        b.photoDirectory(new File(Constants.getImagesTempPath()));
        b.useSingleShotMode(true);
        f.setHost(b.build());
    }

    /**
     * Checks that the CameraFragment exists and is visible to the user,
     * then takes a picture.
     */
    public void takePicture() {
        if (f != null && f.isVisible()) {
            f.takePicture(true, false);
        }
    }

    /**
     * Enable buttons and checkboxes only while start button is enabled
     * (process has not been started)
     */
    private void switchEnabledButtons(){
        boolean isEnabled = stopButton.isEnabled();
        startButton.setEnabled(isEnabled);
        stopButton.setEnabled(!isEnabled);
        imageProcessing.setEnabled(isEnabled);
        dropdownXml.setEnabled(isEnabled);
    }

    /*
    Click on options menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (!runningState) {
            int id = item.getItemId();
            if (id == R.id.action_settings) {
                startActivityForResult(new Intent(this, SetSettingsActivity.class), 2);
                return true;
            }
            else if (id == R.id.action_abl) {
                String s = "Lev: " + getBatLevel() + ", ApproxLev: " + getApproxBatLevel() + ", Volt: " + getVoltage() + ", Temp: " + getTemp();
                Log.i("BATTERY", s);
                addNewText("BATTERY: "+s);
                return true;
            }else if(id == R.id.action_about){
                AboutDialog about = new AboutDialog(this);
                about.setTitle("About this app");
                about.show();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
        return false;
    }

    /*
    Returning back from another activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loadPref();
    }

    private void loadPref(){
        SharedPreferences mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Constants.ASTRO_DETECTOR_PATH = mySharedPreferences.getString(SettingsActivity.FOLDER_PATH, "");
        Constants.PICTURE_SIZE = Integer.parseInt(mySharedPreferences.getString(SettingsActivity.PICTURE_SIZES, ""));
        Constants.USE_THREADS = mySharedPreferences.getBoolean(SettingsActivity.USE_THREADS, true);
        Constants.JPEG_COMPRESSION = Integer.parseInt(mySharedPreferences.getString(SettingsActivity.JPEG_COMPRESSION, ""));
        Constants.LOGGING = mySharedPreferences.getBoolean(SettingsActivity.USE_LOGGING, true);
        Constants.LINE_THRESHOLD = Double.parseDouble(mySharedPreferences.getString(SettingsActivity.LINE_THRESHOLD, ""));
        Constants.LINE_DETECT_METHOD = Integer.parseInt(mySharedPreferences.getString(SettingsActivity.LINE_DETECT_METHOD, ""));
        if (imageProcessing.isActivated())
            loadCameraFragment(false);
    }

    /*
    Dropdown Box
     */
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        chosenXML = (String) adapterView.getItemAtPosition(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    /**
     * Handle onReceive events from the background process messages
     */
    public class BroadcasterReceiverTextView extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String text = intent.getExtras().getString(Constants.RESPONSE);
            if (text.contains("stopped")){
                if (imageProcessing.isChecked()) {
                    //Turn sound back on
                    AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    manager.setStreamVolume(AudioManager.STREAM_SYSTEM, volume , AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                    if (!picturesProcess.isCancelled())
                        picturesProcess.cancel(true);
                }
                stopService(resultViewIntent);
                unregisterReceiver(broadcasterReceiverTextView);
                hideLoadingAnimation();
                switchEnabledButtons();
                runningState = false;
                text = "Battery change: " + batteryLevel + "\n" + text;
            }

            batteryLevel -= getApproxBatLevel();
            addNewText(text);
        }
    }

    /**
     * Add some given text to the text view and scroll the view down to the newest entry.
     * @param text
     */
    public void addNewText(String text){
        resultTextView.setText(resultTextView.getText() + "\n" + text);
        resultScrollView.post(new Runnable() {
            @Override
            public void run() {
                resultScrollView.scrollTo(0, resultTextView.getHeight());
            }
        });
    }

    //Animation while the last steps of Streams Process are done
    //show
    public void showLoadingAnimation()
    {
        pageLoading.setVisibility(View.VISIBLE);
        stopButton.setEnabled(false);
    }


    //hide
    public void hideLoadingAnimation()
    {
        pageLoading.setVisibility(View.GONE);
        stopButton.setEnabled(true);
    }

    /*
     * Battery information
     */
    public double getApproxBatLevel(){
        double v = getVoltage();
        double t = getTemp();

        return t*0.10011667232667519
                +v*126233.59218389183
                +Math.log(v)*-1.2296821324138385E8
                +Math.log(t)*-19.89147951465004
                +v*v*-24.28275192730815
                +v*v*v*0.002766478317172413
                +v*v*v*v*-1.328919843469256E-7
                +7.6046003052751E8;
    }

    public int getBatLevel(){
        initBatIntent();
        return batIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
    }

    public int getVoltage(){
        initBatIntent();
        return batIntent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
    }

    public int getTemp(){
        initBatIntent();
        return batIntent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
    }

    private void initBatIntent(){
        iF = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        batIntent = this.registerReceiver(null, iF);
    }
}

