package sfb876.astrodetector.streams.io;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;

import sfb876.astrodetector.app.Constants;
import sfb876.astrodetector.streams.PrintOutput;
import stream.Data;
import stream.data.DataFactory;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;
import stream.io.AbstractStream;

/**
 * @author Alexey Egorov
 * Created by alexey on 04.09.14.
 */
public class AndroidCameraStream extends AbstractStream {

    final static Logger log = LoggerFactory.getLogger(AndroidCameraStream.class);
    private static String previousFileName = "";
    private File filePath;
    public static boolean stopCameraStream = false;

    @Override
    public void init() throws Exception {
        super.init();

        log.debug("Initializing camera stream.");
        filePath = new File(Constants.getImagesTempPath());
        if (!filePath.exists()){
            if (filePath.mkdirs()){
                log.debug("Folder " + filePath + " created.");
            }else{
                log.error("Error while trying to create a folder.");
                throw new FileNotFoundException("Error while trying to create a folder.");
            }
        }
        stopCameraStream = false;
        log.debug("Camera stream initialized.");
    }

    @Override
    public Data readNext() throws Exception {

        Data item = null;

        int c = 15;
        while (c-- > 0 && !stopCameraStream) {

            File[] listOfFiles = filePath.listFiles();
            File nextFile = findNextFile(listOfFiles);
            if (nextFile != null) {
                item = DataFactory.create();
                item = fillDataItem(nextFile, item);
                break;
            }
            log.debug("WAITING FOR FILES...");
            Thread.sleep(Constants.WAITING_FOR_FILES);
        }
        return item;
    }

    /**
     * Try to find some file in the file list to be different to previously used one.
     * In case such a file couldn't have been found, null is returned.
     * @param listOfFiles
     * @return
     */
    private File findNextFile(File[] listOfFiles){
        File nextFile = null;
        if (listOfFiles!=null) {
            for (File file : listOfFiles) {
                if (!previousFileName.equals(file.toString()) && file != null) {
                    nextFile = file;
                    previousFileName = file.toString();
                    break;
                }
            }
        }
        return nextFile;
    }

    /**
     * Fill the data item with image bitmap decoded from file and some further details to that file.
     * @param nextFile image file
     * @param item data item to be filled
     * @return Data item
     */
    private Data fillDataItem(File nextFile, Data item){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(nextFile.getAbsolutePath(), options);
        if (bitmap != null) {
            item.put("frame:size_raw", nextFile.length());
            item.put("frame:data", nextFile);
            item.put("frame:width", bitmap.getWidth());
            item.put("frame:height", bitmap.getHeight());

            int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
            bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
            ImageRGB img = new ImageRGB(bitmap.getWidth(), bitmap.getHeight(), pixels);
            item.put(AbstractImageProcessor.DEFAULT_IMAGE_KEY, img);
        }
        nextFile.delete();
        return item;
    }
}
