package sfb876.astrodetector.streams.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import stream.Data;

/**
 *
 * @author Marcel Fitzner, Alexey Egorov
 */
public class JpegCommentReader {

    static Logger log = LoggerFactory.getLogger(JpegCommentReader.class);
    public final static byte[] JPEG_COMMENT = new byte[] { (byte) 0xff, (byte) 0xfe };

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    final InputStream in;
    final byte[] buf;
    final ByteBuffer current;

    /**
     * This class should help to detect a comment field inside a byte array which is a JPEG image in real.
     * Furthermore it contains a method @{setClassificationData}
     * @param data byte array of data
     * @param readBufferSize
     * @param maxResultBuffer
     */
    public JpegCommentReader(byte[] data, int readBufferSize, int maxResultBuffer) {
        this.in = new ByteArrayInputStream(data);
        this.buf = new byte[readBufferSize];

        current = ByteBuffer.allocateDirect(maxResultBuffer);
        current.clear();
        if (readBufferSize > maxResultBuffer)
            throw new RuntimeException(
                    "Result buffer cannot be smaller than read-buffer!");
    }

    /**
     * Split the data from the comment right and put them into the data item.
     * @param item Data map
     * @param list list of attribute names
     * @param comment comment to be parsed
     */
    public static void setClassificationData (Data item, String[] list, String comment){
        if (list!=null && item!=null){
            String[] splitComment = comment.split(",");
            for (String str : splitComment){
                String[] splitStr = str.split(":");
                item.put(splitStr[0].toLowerCase(), Integer.parseInt(splitStr[1]));
            }
        }
    }

    private int getCommentLength(int pos) {

        int len = -1;
        byte[] b = new byte[2];
        b[0] = current.get(pos + 2);
        b[1] = current.get(pos + 3);

        String hex = bytesToHex(b);

        try {
            len = Integer.parseInt(hex, 16) - 2; // subtract 2 bytes used for the length indicator
        }
        catch ( NumberFormatException e) {
            log.debug("Could not read the length indicator for the jpg's comment!");
        }

        return len;
    }

    private String fetchCommentFromCurrent(int pos, int length) {

        byte[] comment = new byte[length];

        for(int i=0; i<length; i++) {
            comment[i] = current.get(pos + i);
        }

        String retString = "";
        try {
            retString = new String(comment, "UTF-8");
        }
        catch ( UnsupportedEncodingException e) {
            log.debug("this should never happen because \"UTF-8\" is hard-coded..");
        }
        return retString;
    }

    // http://stackoverflow.com/questions/9655181/convert-from-byte-array-to-hex-string-in-java
    private String bytesToHex(byte[] bytes) {

        int len = bytes.length;
        char[] hexChars = new char[len * 2];
        for (int j = 0; j < len; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Try to find comment in a JPEG.
     * @return comment string extracted from the JPEG
     * @throws IOException
     */
    public String searchComment() throws IOException {

        int read = 0;
        int startComment = -1;
        while (startComment < 0 && read >= 0) {
            read = readBytes();
            startComment = indexOf(JPEG_COMMENT, 0);
        }

        String comment = "";
        if (startComment > 0) {
            comment = fetchCommentFromCurrent(startComment + 4, getCommentLength(startComment));
        }
        return comment;
    }

    protected int indexOf(byte[] sig, int from) {
        int pos = from;
        int len = sig.length;
        while (pos + len < current.position()) {
            if (isSignatureAt(sig, pos)) {
                return pos;
            }
            pos++;
        }
        return -1;
    }

    protected boolean isSignatureAt(byte[] sig, int pos) {
        int len = sig.length;
        for (int i = 0; i < len; i++) {
            byte b = current.get(pos + i);
            if (b != sig[i]) {
                return false;
            }
        }
        return true;
    }

    protected int readBytes() throws IOException {

        if (current.limit() - current.position() == 0) {
            log.info("Buffer full!");
            return 0;
        }

        int read = in.read(buf, 0, buf.length);
        if (read < 0)
            return -1;

        current.put(buf, 0, read);
        return read;
    }
}
