package sfb876.astrodetector.streams.io;

import android.os.AsyncTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import sfb876.astrodetector.app.Constants;
import sfb876.astrodetector.app.StartActivity;

/**
 * Created by alexey on 04.09.14.
 */
public class TakePicturesProcess extends AsyncTask<Void, Void, Void> {

    final static Logger log = LoggerFactory.getLogger(TakePicturesProcess.class);
    private static boolean photoDone = true;
    private static StartActivity startActivity;

    public static StartActivity getStartActivity() {
        return startActivity;
    }
    public static void setStartActivity(StartActivity start){
        startActivity = start;
    }

    public static boolean isPhotoDone() {
        return photoDone;
    }

    public static void setPhotoDone(boolean photoDone) {
        TakePicturesProcess.photoDone = photoDone;
    }

    @Override
    protected Void doInBackground(Void... v) {
        if (startActivity==null){
            log.error("ERROR: Picture can not been taken, until StartActivity (caller) for TakePicturesProcess is not set.");
            return null;
        }
        File folder = new File(Constants.getImagesTempPath());

        if (!Constants.createFolder(folder))
            Constants.removeFolderContent(folder);

        try {
            Thread.sleep(Constants.CAMERA_LOAD_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final int waiting_time = Constants.WAITING_FOR_PHOTO_DONE;
        while(!isCancelled()) {
            int timer = 0;
            while(!isPhotoDone()) {
                try {
                    Thread.sleep(waiting_time);
                    timer+=waiting_time;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            log.debug("Timer have slept for " + (timer + waiting_time) * 0.001 + " seconds");
            synchronized (AndroidCameraHost.getObject()){
                setPhotoDone(false);
                try {
                    Thread.sleep(waiting_time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startActivity.takePicture();
                Constants.PREVIEW_RESTARTED = false;
            }
        }
        return null;
    }

}