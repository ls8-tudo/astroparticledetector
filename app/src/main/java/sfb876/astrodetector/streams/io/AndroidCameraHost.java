package sfb876.astrodetector.streams.io;

import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;

import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;

import sfb876.astrodetector.app.Constants;

/**
 * @author Alexey Egorov
 * Created by alexey on 05.09.14.
 */
public class AndroidCameraHost extends SimpleCameraHost {

    final static Logger log = LoggerFactory.getLogger(AndroidCameraHost.class);

    /**
     * Sync-object to reset the value of photoDone variable
     */
    private static Object object = new Object();
    public static Object getObject() {
        return object;
    }

    public AndroidCameraHost(Context _ctxt) {
        super(_ctxt);
    }

    @Override
    public Camera.Size getPictureSize(PictureTransaction xact, Camera.Parameters parameters) {
        return parameters.getSupportedPictureSizes().get(Constants.PICTURE_SIZE);
    }

    @Override
    public void saveImage(PictureTransaction xact, Bitmap bitmap) {
        File outputFile = new File(Constants.getImagesTempPath());
        Constants.createFolder(outputFile);
        outputFile = new File(outputFile, "Photo_" + System.currentTimeMillis() + ".jpeg");
        try {
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, Constants.JPEG_COMPRESSION, fos);
            fos.close();

        } catch (Exception e){
            log.error("Error with saving the Image "+ e.getMessage() + "\n" + e.getCause());
        }
        synchronized (getObject()) {
            TakePicturesProcess.getStartActivity().getCameraFragment().restartPreview();
            Constants.PREVIEW_RESTARTED = true;
            TakePicturesProcess.setPhotoDone(true);
        }
    }
}
