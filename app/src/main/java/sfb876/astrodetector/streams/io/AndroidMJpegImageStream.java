package sfb876.astrodetector.streams.io;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import stream.Data;
import stream.data.DataFactory;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;
import stream.io.AbstractStream;
import stream.io.JpegStream;
import stream.io.SourceURL;
import stream.util.ByteSize;

/**
 * @author chris
 *
 */
public class AndroidMJpegImageStream extends AbstractStream {
    static Logger log = LoggerFactory.getLogger(AndroidMJpegImageStream.class);

    protected JpegStream stream;
    protected ByteSize readBufferSize = new ByteSize("1k");
    protected ByteSize bufferSize = new ByteSize("16mb");
    protected boolean continuous = false;
    public static boolean stopJpegStream = false;
    int ok = 0;
    int errors = 0;

    public AndroidMJpegImageStream(SourceURL url) throws Exception {
        super(url);
    }

    public AndroidMJpegImageStream(InputStream in) throws Exception {
        super(in);
    }

    /**
     * @return the readBufferSize
     */
    public ByteSize getReadBufferSize() {
        return readBufferSize;
    }

    /**
     * @param readBufferSize
     *            the readBufferSize to set
     */
    public void setReadBufferSize(ByteSize readBufferSize) {
        this.readBufferSize = readBufferSize;
    }

    /**
     * @return the bufferSize
     */
    public ByteSize getBufferSize() {
        return bufferSize;
    }

    /**
     * @param bufferSize
     *            the bufferSize to set
     */
    public void setBufferSize(ByteSize bufferSize) {
        this.bufferSize = bufferSize;
    }

    /**
     * @return the continuous
     */
    public boolean isContinuous() {
        return continuous;
    }

    /**
     * @param continuous
     *            the continuous to set
     */
    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    /**
     * @see stream.io.AbstractStream#init()
     */
    @Override
    public void init() throws Exception {
        super.init();

        log.debug("Initializing stream...");
        stream = new JpegStream(getInputStream(), readBufferSize.getBytes(),
                bufferSize.getBytes(), isContinuous());
        stopJpegStream = false;
        log.debug("Stream initialized.");
    }

    /**
     * @see stream.io.AbstractStream#readNext()
     */
    @Override
    public Data readNext() throws Exception {

        if (stopJpegStream)
            return null;

        byte[] data = stream.readNextChunk();

        if (data == null)
            return null;

        String comment =
                new JpegCommentReader(data, readBufferSize.getBytes(), bufferSize.getBytes())
                        .searchComment();

        log.info("Comment: " + comment);

        Data item = DataFactory.create();
        item.put("frame:size_raw", data.length);

        String[] list = {"Worm:", "x1:", "y1:", "x2:", "y2:"};
        JpegCommentReader.setClassificationData(item, list, comment);

        try {
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            int[]pixels = new int[bitmap.getHeight() * bitmap.getWidth()];
            bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
            ImageRGB img = new ImageRGB(bitmap.getWidth(), bitmap.getHeight(), pixels);
            item.put(AbstractImageProcessor.DEFAULT_IMAGE_KEY, img);
            item.put("frame:width", bitmap.getWidth());
            item.put("frame:height", bitmap.getHeight());
            ok++;
            log.debug("Successfully parsed JPEG image...");
        } catch (Exception e) {
            log.error("Failed to read from image: {}", e.getMessage());
            errors++;
            if (log.isDebugEnabled())
                e.printStackTrace();
            item.put("error:data", data);
        }
        return item;
    }

    /**
     * @see stream.io.AbstractStream#close()
     */
    @Override
    public void close() throws Exception {
        super.close();
    }

}