package sfb876.astrodetector.streams.imageEditing;

import android.graphics.Bitmap;
import android.os.Environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sfb876.astrodetector.app.Constants;
import sfb876.astrodetector.streams.PrintOutput;
import stream.Data;
import stream.annotations.Parameter;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;
import stream.io.AbstractStream;

import java.io.*;

/**
 * Created by alexey on 29.08.14.
 */
public class SaveImage extends AbstractImageProcessor {

    final static Logger log = LoggerFactory.getLogger(AbstractStream.class);

    static int counter = 0;
    String location = "SavedImages";

    public void setImageKey(String imageKey){
        super.imageKey = imageKey;
    }

    public String getLocation() {
        return location;
    }

    @Parameter(description = "Location where the image should be saved.")
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public Data process(Data data, ImageRGB img) {

        if (img!=null) {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + Constants.ASTRO_DETECTOR_PATH + "SavedPictures/";
            File outputFile = new File(path);
            if (!outputFile.exists()) {
                try {
                    if (!outputFile.createNewFile()) {
                        String output = (String) data.get(PrintOutput.PRINT_OUTPUT);
                        data.put(PrintOutput.PRINT_OUTPUT,
                                (output != null ? output : "")
                                        + "Folder \"" + outputFile + "\"could not have been created."
                                        + "\n");
                        return data;
                    }
                } catch (IOException e) {
                    log.error("Error with saving the Image {} \n{}", e.getMessage(), e.getCause().toString());
                }
            }
            outputFile.mkdirs();

            Long id = (Long) data.get("@id");
            id = id!=null ? id : counter;
            outputFile = new File(outputFile, String.format("%03d", id) + "-" + location + ".jpeg");
            OutputStream out = null;
            try {
                if (!outputFile.exists()) {
                    if (!outputFile.createNewFile()) {
                        String output = (String) data.get(PrintOutput.PRINT_OUTPUT);
                        data.put(PrintOutput.PRINT_OUTPUT,
                                (output != null ? output : "")
                                        + "File \"" + outputFile + "\" could not have been created."
                                        + "\n");
                        return data;
                    }
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                out = new BufferedOutputStream(fos);
                Bitmap.createBitmap(img.pixels, img.width, img.height, Bitmap.Config.ARGB_8888).compress(Bitmap.CompressFormat.JPEG, 100, out);

            } catch (Exception e) {
                log.error("Error with saving the Image {} \n{}", e.getMessage(), e.getCause().toString());
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        String output = (String) data.get(PrintOutput.PRINT_OUTPUT);
                        data.put(PrintOutput.PRINT_OUTPUT,
                                (output != null ? output : "")
                                        + "File \"" + outputFile + "\" could not have been created."
                                        + "\n");
                    }
                }
            }
        }
        counter++;
        return data;
    }

}
