package sfb876.astrodetector.streams.imageEditing;

/**
 * Created by alexey on 08.09.14.
 */
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import stream.Data;
import stream.annotations.Parameter;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;

public class ColorToGrayscale extends AbstractImageProcessor{

    String output = AbstractImageProcessor.DEFAULT_IMAGE_KEY;

    public void setImageKey(String imageKey){
        super.imageKey = imageKey;
    }

    @Parameter(description="The name/key under which the output image is stored. If this name equals the name of the input image, the input image is going to be overwritten.")
    public void setOutput(String output) {
        this.output = output;
    }

    @Override
    public Data process(Data item, ImageRGB img) {

        ImageRGB newImg = new ImageRGB(img.width, img.height);

        Bitmap bit = Bitmap.createBitmap(img.width, img.height, Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bit);
        Paint p = new Paint();
        ColorMatrix cm = new ColorMatrix();

        cm.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(cm);
        p.setColorFilter(filter);
        c.drawBitmap(Bitmap.createBitmap(img.pixels, img.width, img.height, Bitmap.Config.ARGB_8888), 0, 0, p);

        bit.getPixels(newImg.pixels, 0, img.width, 0, 0, img.width, img.height);

        item.put(output, newImg);
        return item;
    }
}
