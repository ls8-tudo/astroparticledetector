/**
 *
 */
package sfb876.astrodetector.streams.imageEditing;

import android.util.Log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.ProcessContext;
import stream.annotations.Parameter;
import stream.expressions.version2.DoubleExpression;
import stream.expressions.version2.Expression;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;

/**
 * @author Alexey
 *
 */
public class Crop extends AbstractImageProcessor {

    final static Logger log = LoggerFactory.getLogger(Crop.class);
    String output = "frame:cropped";
    String x = "0";
    String y = "0";
    String width = "10";
    String height = "10";
    Expression<Double> widthDouble;
    Expression<Double> heightDouble;
    Expression<Double> xDouble;
    Expression<Double> yDouble;

    public void setImageKey(String imageKey){
        super.imageKey = imageKey;
    }

    public String getOutput() {
        return output;
    }

    @Parameter(description = "Key/name of the attribute into which the output cropped image is placed, default is 'frame:cropped'.")
    public void setOutput(String output) {
        this.output = output;
    }

    public String getX() {
        return x;
    }

    @Parameter(description = "x coordinate of the lower-left corder of the rectangle for cropping, defaults to 0.")
    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    @Parameter(description = "y coordinate of the lower-left corder of the rectangle for cropping, defaults to 0.")
    public void setY(String y) {
        this.y = y;
    }

    public String getWidth() {
        return width;
    }

    @Parameter(description = "Width of the rectangle to crop, default is 10.")
    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    @Parameter(description = "Height of the rectangle to crop, default is 10.")
    public void setHeight(String height) {
        this.height = height;
    }


    @Override
    public void init(ProcessContext ctx) throws Exception {
        super.init(ctx);
        widthDouble = new DoubleExpression(width);
        heightDouble = new DoubleExpression(height);
        xDouble = new DoubleExpression(x);
        yDouble = new DoubleExpression(y);
    }

    @Override
    public Data process(Data item, ImageRGB img) {

        int xInt = 0;
        int yInt = 0;
        int widthInt = 0;
        int heightInt = 0;
        try {
            xInt = (int) Math.round(xDouble.get(context, item));
            yInt = (int) Math.round(yDouble.get(context, item));
            widthInt = (int) Math.round(widthDouble.get(context, item));
            heightInt = (int) Math.round(heightDouble.get(context, item));
        } catch (Exception e) {
            e.printStackTrace();
        }

        int[] pixelsCropped = new int[widthInt*heightInt];
        try {
            for (int j = 0; j < heightInt; j++) {
                int widJ = widthInt*j;
                for (int i = 0; i < widthInt; i++) {
                    pixelsCropped[widJ + i] = img.pixels[img.width * (j+yInt) + (xInt+i)];
                }
            }
        } catch (Exception e) {
            Log.e(this.getClass().toString(), "Failed to crop image: " + e.getMessage());
            if (log.isDebugEnabled())
                e.printStackTrace();
        }
        item.put(output, new ImageRGB(widthInt, heightInt, pixelsCropped));
        return item;
    }

}
