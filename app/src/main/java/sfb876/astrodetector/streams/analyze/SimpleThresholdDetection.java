package sfb876.astrodetector.streams.analyze;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import sfb876.astrodetector.streams.PrintOutput;
import stream.Data;
import stream.image.AbstractImageProcessor;
import stream.image.ImageRGB;

/**
 * Created by Matuschek on 31.07.2014.
 */
public class SimpleThresholdDetection extends AbstractImageProcessor {

    final static Logger log = LoggerFactory.getLogger(SimpleThresholdDetection.class);
    double threshold = .0;

    public void setImageKey(String imageKey){
        super.imageKey = imageKey;
    }

    public void setThreshold(double threshold) {
        if (threshold > 1.0)
            threshold = 1.0;
        if (threshold < 0.0)
            threshold = 0.0;
        this.threshold = threshold;
    }

    @Override
    public Data process(Data item, ImageRGB img) {

        boolean detected = false;

        int points = 0;
        int xRight = 0;
        int xLeft = img.width;
        int yTop = 0;
        int yDown = img.height;

        short maxPoints = 700;
        double oneThird = 1 / 3.0;

        int intThreshold = (int) (255 * threshold);

        for (int y = 0; y < img.height && maxPoints > 0; y++) {

            final int widY = img.width * y;
            for (int x = 0; x < img.width && maxPoints > 0; x++) {

                int pixel = img.pixels[widY + x];

                int red = (pixel >> 16) & 0xFF;
                int green = (pixel >> 8) & 0xFF;
                int blue = pixel & 0xFF;

                int grey = (int) ((red + green + blue) * oneThird);

                if (grey > intThreshold) {
                    xRight = xRight < x ? x : xRight;
                    xLeft = xLeft > x ? x : xLeft;
                    yTop = yTop < y ? y : yTop;
                    yDown = yDown > y ? y : yDown;
                    points++;
                    maxPoints--;
                }
            }
        }
        if (points > 3) {
            detected = true;
            File file = (File) item.get("frame:data");
            String filePath = "";
            if (file != null) {
                filePath = "\n" + file.toString();
            }
            item.put("cropped_x", xLeft);
            item.put("cropped_y", yDown);
            item.put("cropped_width", Math.abs(xLeft - xRight));
            item.put("cropped_height", Math.abs(yTop - yDown));
            item.put("threshold", intThreshold);

            String output = (String) item.get(PrintOutput.PRINT_OUTPUT);
            output = (output != null ? output : "")
                    + "Event detected by SimpleThresholdDetection"
                    + filePath + "\nList size: " + points;
            item.put(PrintOutput.PRINT_OUTPUT, output);
            log.info(output);
        }
        item.put("detected", detected);
        return item;
    }
}
