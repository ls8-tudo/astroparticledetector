package sfb876.astrodetector.streams;

import java.io.File;

import stream.run;

/**
 * Created by Matuschek on 26.09.2014.
 */
public class StartOnPc {

    public static void main(String [ ] args) {
        try {
            // Set your own path to the xml file of the streams process
            run.main(new File("app/src/main/resources/DetectOnPC.xml").toURI().toURL());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
