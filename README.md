# Astro Particle Detector #

This is a repository prepared for [Summer school 2014](http://sfb876.tu-dortmund.de/SummerSchool2014/index.html).
While the real intention behind this application is to detect some astro particles, in this context it is meant to be used for testing and learning while a workshop about [streams framework](http://www.jwall.org/streams/).
For a real application already working on detecting astro particles please visit [DECO website](http://www.sensorcast.org/wiki/index.php/DECO).

You will find an Android Studio project in there.
To work with it you need:

* JDK 7 and higher

* Android Studio version 0.8.6 and higher (please, do update after first launch directly)

All dependencies to work with *streams framework* are automatically built by gradle during the first start.